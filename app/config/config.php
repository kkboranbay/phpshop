<?php

function __autoload($className)
{
    $path = str_replace("_", "/", $className);
    include_once("../" . $path . ".php");
}

define('HOST', 'localhost');
define('USER', 'root');
define('PASS', '');
define('DB', 'phpShop');

$con = mysqli_connect(HOST, USER, PASS, DB);

if (mysqli_connect_errno()){
    echo "Failed to connect: " . mysqli_connect_error();
}

mysqli_close($con);





