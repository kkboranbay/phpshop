<?php

class Lib_Menu
{

    protected function __clone(){}
    protected function __wakeup(){}
    protected function __construct(){}

    protected static $instance = null;
    public static function getInstance()
    {
        if (is_null(static::$instance)){
            static::$instance = new static;
        }
        return static::$instance;
    }

    public $menu = [
        "Home" => "/app/web",
        "Catalog" => "catalog",
        "Login" => "login"
    ];

    public function getMenu()
    {
        $print = "<ul class=\"navbar-nav ml-auto\">";
        foreach ($this->menu as $name => $path){
            $print .= "<li class=\"nav-item\"><a class=\"nav-link\" href='$path'>" . $name . "</a></li>";
        }
        $print .= "</ul>";
        return $print;
    }

}